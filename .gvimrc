" threeve's .gvimrc
"
" Author: Jason Foreman <jason@threeve.org>

" Use one of these preferred fonts
set guifont=Menlo:h12,Consolas:h12,Inconsolata:h12

set guioptions=
set guioptions+=a	" integrate with OS pasteboard
set guioptions+=A	" same for modeless selection
set guioptions+=c	" console dialogs
set guioptions+=e	" GUI tab pages
